import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
import joblib

# Cargar los datos
df = pd.read_csv('/content/Comandos.csv')

# Crear el pipeline de scikit-learn  El pipeline consta de dos etapas
pipeline = Pipeline([
    ('tfidf', TfidfVectorizer()), #Convierte el texto en matriz, Divide el texto en palabras (tokens), Construye un diccionario de todas las palabras encontradas en el conjunto de datos.
    ('clf', LogisticRegression()) #Usa clasificación binaria, predice la probabilidad de que una instancia pertenezca a una clase particular. "El comando a la descripcion"
])
#Un pipeline permite encadenar varios pasos de procesamiento, asegurando que la salida de cada paso sea la entrada del siguiente.

# Entrenar el modelo
pipeline.fit(df['Uso'], df['Comando']) #Toma la columna 'uso' como caracteristicas y 'comando' como etiquetas. Aprende a predecir el comando Git basado en descripciones en lenguaje natural.

# Guardar el modelo entrenado
joblib.dump(pipeline, 'Modelo_GIT.joblib')