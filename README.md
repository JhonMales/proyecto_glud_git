# Proyecto_GLUD_GIT

## Descripción
Este proyecto es un traductor de comandos de Git basado en una interfaz gráfica simple usando `tkinter`. Utiliza un modelo de aprendizaje automático entrenado para predecir el comando de Git más adecuado basado en la descripción proporcionada por el usuario.

## Requisitos
- Python 3.6 o superior
- pip (gestor de paquetes de Python)

## Instalación
Para instalar las librerías necesarias en Linux, siga los siguientes pasos:

1. **Actualizar el sistema y asegurar que pip está instalado:**
    ```bash
    sudo apt update
    sudo apt install python3-pip
    ```

2. **Instalar las librerías necesarias:**
    ```bash
    pip3 install pandas scikit-learn joblib
    ```

3. **Instalar `tkinter`:**
    ```bash
    sudo apt-get install python3-tk
    ```

## Uso
1. **Navegar al directorio donde se encuentra el script `Proyecto.py`:**
    ```bash
    cd Descargas/
    ```

2. **Ejecutar el script:**
    ```bash
    python3 Proyecto.py
    ```

## Estructura del proyecto
El proyecto incluye los siguientes archivos:
- `Entrenamiento.py`: Script que contiene el codigo de entrenamiento para el modelo de prediccion, hace uso de Comandos.csv
- `Comandos.csv`: Archivo CSV con las descripciones de uso y los comandos de Git.
- `Modelo_GIT.joblib`: Modelo de aprendizaje automático entrenado.
- `Proyecto.py`: Script principal que contiene el código de la interfaz gráfica y la lógica para predecir comandos de Git.

## Autor
- Jhon Elvis Males 20191020144
- Cristian Espitia 20192020104
