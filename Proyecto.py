import tkinter as tk
from tkinter import messagebox
import joblib

# Cargar el modelo entrenado
pipeline = joblib.load('./Modelo_GIT.joblib')


def translate_to_git_command(user_input, model):
    git_command = model.predict([user_input])[0]
    return git_command

def on_submit():
    user_input = entry.get()
    if user_input.lower() == 'salir':
        root.quit()
    else:
        git_command = translate_to_git_command(user_input, pipeline)
        result_label.config(text=f"Comando Git: {git_command}")

# Crear la ventana principal
root = tk.Tk()
root.title("Traductor de Comandos Git")

# Crear un campo de entrada
entry = tk.Entry(root, width=50)
entry.pack(pady=10)

# Crear un botón de enviar
submit_button = tk.Button(root, text="Enviar", command=on_submit)
submit_button.pack(pady=5)

# Crear un label para mostrar el resultado
result_label = tk.Label(root, text="")
result_label.pack(pady=10)

# Iniciar el bucle principal
root.mainloop()